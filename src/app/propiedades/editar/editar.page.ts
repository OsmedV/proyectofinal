import { Component, OnInit } from "@angular/core";
import { propiedades } from "../propiedades.model";
import { PropiedadesService } from "../propiedades.service";
import { Location } from "@angular/common";
import { AlertController } from "@ionic/angular";
import {
  Resolve,
  ActivatedRouteSnapshot,
  ActivatedRoute,
} from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-editar",
  templateUrl: "./editar.page.html",
  styleUrls: ["./editar.page.scss"],
})
export class EditarPage implements OnInit {
  public id: string;
  public item: any;
  public form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private servicio: PropiedadesService,
    private _location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private alert: AlertController
  ) {}

  ngOnInit() {
    this.route.data.subscribe((routeData) => {
      let data = routeData["data"];
      if (data) {
        this.item = data.payload.data();
        this.item.id = data.payload.id;

        this.createForm();
      }
    }); 
  }

  //Funcion crear formulario
  createForm() {
    this.form = this.fb.group({
      nombre: new FormControl(this.item.nombre, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      descripcion: new FormControl(this.item.descripcion, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      ubicacion: new FormControl(this.item.ubicacion, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      //map comodidades below
      ac: new FormControl(this.item.comodidades.ac, {
        updateOn: "blur",
      }),
      cocina: new FormControl(this.item.comodidades.cocina, {
        updateOn: "blur",
      }),
      mascotas: new FormControl(this.item.comodidades.mascotas, {
        updateOn: "blur",
      }),
      piscina: new FormControl(this.item.comodidades.piscina, {
        updateOn: "blur",
      }),
      wifi: new FormControl(this.item.comodidades.wifi, {
        updateOn: "blur",
      }),
      // end map
      precio: new FormControl(this.item.precio, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      imagen: new FormControl(this.item.imagen, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
    });
  }

  //Funcion editar
  onSubmit() {
    this.form.value.ac = !this.form.value.ac ? false : true;
    this.form.value.cocina = !this.form.value.cocina ? false : true;
    this.form.value.mascotas = !this.form.value.mascotas ? false : true;
    this.form.value.piscina = !this.form.value.piscina ? false : true;
    this.form.value.wifi = !this.form.value.wifi ? false : true;
    this.servicio.editarPropiedades(
      this.item.id,
      this.form.value.nombre,
      this.form.value.descripcion,
      { lat: "51.531982", long: "-0.17725" },
      {
        ac: this.form.value.ac = !this.form.value.ac ? false : true,
        cocina: this.form.value.cocina = !this.form.value.cocina ? false : true,
        mascotas: this.form.value.mascotas = !this.form.value.mascotas
          ? false
          : true,
        piscina: this.form.value.piscina = !this.form.value.piscina
          ? false
          : true,
        wifi: this.form.value.wifi = !this.form.value.wifi ? false : true,
      },
      this.form.value.precio,
      this.form.value.imagen
    );
    console.log(this.form);
    window.history.back();
  }

  //Funcion cancelar
  cancel() {
    this.router.navigate(["/home"]);
  }
}
