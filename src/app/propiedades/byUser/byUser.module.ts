import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ByUserPageRoutingModule } from './byUser-routing.module';

import { ByUserPage } from './byUser.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    ByUserPageRoutingModule
  ],
  declarations: [ByUserPage]
})
export class ByUserPageModule {}
