import { Component, OnInit } from "@angular/core";
import { PropiedadesService } from "../propiedades.service";
import { Location } from "@angular/common";
import { Router} from "@angular/router";

@Component({
  selector: "byUser",
  templateUrl: "./byUser.page.html",
  styleUrls: ["./byUser.page.scss"],
})
export class ByUserPage implements OnInit {

  public items: any;
  
  constructor(
    private servicio: PropiedadesService,
    private router: Router
    ) {} 

  ngOnInit() {
    this.display();
  }

  display() {
    this.items = this.servicio.getByUser();
    console.log(this.items); 
  }
  goEdit(item){
    this.router.navigate(['/propiedades/editar/'+ item]);
  }

  viewDetails(item){
    this.router.navigate(['/propiedades/detalle/'+ item]);
  }
}
