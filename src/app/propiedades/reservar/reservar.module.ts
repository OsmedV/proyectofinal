import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservarPageRoutingModule } from './reservar-routing.module';

import { ReservarPage } from './reservar.page';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    ReservarPageRoutingModule, 
    NgbModule, 
    FormsModule
  ],
  declarations: [ReservarPage],
  exports: [ReservarPage],
  bootstrap: [ReservarPage]
})
export class ReservarPageModule {}
