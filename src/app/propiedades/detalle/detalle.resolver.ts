import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";
import { PropiedadesService } from "../propiedades.service";

@Injectable()
export class DetalleResolver implements Resolve<any> {

  constructor(private servicio: PropiedadesService) { }

  resolve(route: ActivatedRouteSnapshot,) {

    return new Promise((resolve, reject) => {
      let id = route.paramMap.get('idPropiedad');
      console.log(route.paramMap);
      this.servicio.getPropiedad(id)
      .subscribe(
        data => {
          resolve(data);
        }
      );
    })
  }
}