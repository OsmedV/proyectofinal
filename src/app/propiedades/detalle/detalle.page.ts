import { Component, OnInit } from "@angular/core";
import { propiedades } from "../propiedades.model";
import { PropiedadesService } from "../propiedades.service";
import { Location } from "@angular/common";
import { AlertController } from "@ionic/angular";
import {
  Resolve,
  ActivatedRouteSnapshot,
  ActivatedRoute,
} from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-detalle",
  templateUrl: "./detalle.page.html",
  styleUrls: ["./detalle.page.scss"],
})
export class DetallePage implements OnInit {
 
  public id: string;
  public item: any;
  public form: FormGroup;
  ac: boolean = false; cocina: boolean = false; mascotas: boolean = false; piscina: boolean = false; wifi: boolean = false;
  constructor(
    private fb: FormBuilder,
    private servicio: PropiedadesService,
    private _location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private alert: AlertController
  ) {}

  ngOnInit() {
    this.route.data.subscribe((routeData) => {
      let data = routeData["data"];
      if (data) {
        this.item = data.payload.data();
        this.item.id = data.payload.id;

        this.createForm();
      }
    });
  }

  //Funcion crear formulario
  createForm() {
    if(this.item.comodidades.ac){
      this.ac = true;
    };
    if(this.item.comodidades.cocina){
      this.cocina = true;
    };
    if(this.item.comodidades.mascotas){
      this.mascotas = true;
    };
    if(this.item.comodidades.piscina){
      this.piscina = true;
    };
    if(this.item.comodidades.wifi){
      this.wifi = true;
    };
  }
}