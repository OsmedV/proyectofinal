import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from 'firebase';
import 'rxjs/add/operator/toPromise';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: "root",
})
export class PropiedadesService {
  constructor(public db: AngularFirestore) {}

  //funcion crear propiedad.
  createPropiedad(
    nombre: string,
    descripcion: string,
    ubicacion: any,
    comodidades: any,
    precio: number,
    imagen: string
  ) {
    return this.db.collection("propiedades").add({
      nombre: nombre,
      descripcion: descripcion,
      ubicacion: ubicacion,
      comodidades: comodidades,
      precio: precio,
      imagen: imagen,
      uid: localStorage.getItem("usuario"),
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
  }

  //funcion seleccionar Propiedad by ID
  getPropiedad(id) {
    return this.db.collection("propiedades").doc(id).snapshotChanges();
  }

  getByUser() {
    var i = 0;
    var lista = [];

    var ref = this.db.collection("propiedades", ref => ref.where("uid", "==", localStorage.getItem("usuario")))
    .get().subscribe(function(doc) {            
      doc.forEach(function (item) {
        lista[i] = item.data();
        lista[i].uid = item.id;
        i++;
      })
    });
    return lista;
  }

  //funcion selecionar todas las propiedades
  getPropiedades() {
    return this.db.collection("propiedades").snapshotChanges();
  }

  editarPropiedades(
    id,
    nombre: string,
    descripcion: string,
    ubicacion: any,
    comodidades: any,
    precio: number,
    imagen: string) {
    return this.db.collection("propiedades").doc(id).update({
      nombre: nombre,
      descripcion: descripcion,
      ubicacion: ubicacion,
      comodidades: comodidades,
      precio: precio,
      imagen: imagen,
      uid: localStorage.getItem("usuario"),
    });
  }

  reservar(id, deFecha, hastaFecha, arregloFechas){
    const list = this.db.collection("propiedades").doc(id).collection("reservaciones");
    
    list.add({ 
      usuario: "Juan",
      desde: deFecha,
      hasta: hastaFecha,
    });

    const list2 = this.db.collection("propiedades").doc(id);
    for(let i = 0; i<arregloFechas.length; i++){
      list2.update({
        fechasReservadas: firebase.firestore.FieldValue.arrayUnion(arregloFechas[i])
      });
    }
    
  }

  getFechasReservadas(id){

    var docRef = this.db.collection("propiedades").doc(id);
    var arreglo:NgbDateStruct[] = [];

    docRef.get().subscribe(function(doc) {
        if (doc.exists) {
          for(let i = 0; i<doc.data().fechasReservadas.length; i++){
            arreglo[i] = doc.data().fechasReservadas[i];
          }
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    });

    return arreglo;

  }

obtenerPropiedades(num){
  var i = 0;
  var lista = [];

  var ref = this.db.collection("propiedades", ref => ref.orderBy("timestamp", "desc").limit(num))
  .get().subscribe(function(doc) {            
    doc.forEach(function (item) {
      lista[i] = item.data();
      lista[i].uid = item.id;      
      i++;
    })
  });
  console.log(lista);
  return lista; 
}

}


